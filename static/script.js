var lead_skills;
var awakenings;
/*	1: Enhanced HP, 2: Enhanced Attack, 3: Enhanced Heal, 4: Reduce Fire Damage, 5: Reduce Water Damage, 6: Reduce Wood Damage, 7: Reduce Light Damage,
	8: Reduce Dark Damage, 9: Auto-Recover, 10: Resistance-Bind, 11: Resistance-Dark, 12: Resistance-Jammers, 13: Resistance-Poison, 14: Enhanced Fire Orbs,
	15: Enhanced Water Orbs, 16: Enhanced Wood Orbs, 17: Enhanced Light Orbs, 18: Enhanced Dark Orbs, 19: Extend Time, 20: Recover Bind, 21: Skill Boost
	22: Enhanced Fire Att., 23: Enhanced Water Att., 24: Enhanced Wood Att., 25: Enhanced Light Att., 26: Enhanced Dark Att., 27: Two-Prong Attack, 28: Resistance-Skill Lock*/
var monst_list;
var matches = ['', '', '', '', '', '']; // Fire, Water, Wood, Light, Dark, Heart.
var running = false;
var ajxRan = [0, 0, []]; // Times to run, times run, list to use in callback.

// What a hideous onload.
$(document).ready( function(){
	// Load the json files.
	// TODO: Local caching.
	$.ajax('https://www.padherder.com/api/leader_skills/', {dataType: 'json', success: function(a, b, c){lead_skills = a;}});
	$.ajax('https://www.padherder.com/api/awakenings/', {dataType: 'json', success: function(a, b, c){awakenings = a;}});
	$.ajax('https://www.padherder.com/api/monsters/', {dataType: 'json', success: function(a, b, c){
		monst_list = a;
		$('div.loader').addClass('hidden');
		$('#team-id-form').removeClass('hidden');
	}});
	
	if (!$('#team-block').hasClass('hidden')){ $('#team-block').addClass('hidden'); }
	if (!$('#results-block').hasClass('hidden')){ $('#results-block').addClass('hidden'); }
	if (!$('#team-id-form').hasClass('hidden')){ $('#team-id-form').addClass('hidden'); }
	
	// Populate select fields.
	for (var i = 0; i <= 4; i++){
		$('select optgroup[label=Element]').append("<option value='elem-" + i + "'>" + elements[i] + "</option>");
	}
	for (var i = 1; i <= 7; i++){
		$('select optgroup[label=Type]').append("<option value='type-" + i + "'>" + types[i] + "</option>");
	}

	$('#team-id-form').on('submit', function(){
		var id = $("#team-id-form input[name=team-id]").val();
		if (!id || running){
			return false;
		}
		
		running = true;
		$.ajax( "https://www.padherder.com/user-api/team/" + id + "/", {dataType: 'json', success: function(a, b, c){
			buildTeam(a);
		}} );
		return false;
	});
	
	// Handler for active radio buttons.
	$('input[name=active-mult]').on('change', function(e){
		$('#active-field').val(e.target.value);
	});
	
	// Damage form handlers.
	$('#dmg-form').on('submit', function(){ return false; });
	$('#dmg-submit').on('click', function(){
		if ($('#enemy-health').val() != 0){
			calcDamage();
		}
		return false;
	});
	
	// Bind functions to update +orb fields.
	$('input[name=fr-match]').on('change', function(e){
		var exp = /^|(([1-9][0-9]*|([1-9][0-9]*)*R)(\+([1-9][0-9]*|([1-9][0-9]*)*R))*)$/;
		var val = e.target.value;
		if (!exp.test(val)){
			return false;
		}
		updatePlusFields('fr');
		matches[0] = val;
	});
	
	$('input[name=wt-match]').on('change', function(e){
		var exp = /^|(([1-9][0-9]*|([1-9][0-9]*)*R)(\+([1-9][0-9]*|([1-9][0-9]*)*R))*)$/;
		var val = e.target.value;
		if (!exp.test(val)){
			return false;
		}
		updatePlusFields('wt');
		matches[1] = val;
	});
	
	$('input[name=wd-match]').on('change', function(e){
		var exp = /^|(([1-9][0-9]*|([1-9][0-9]*)*R)(\+([1-9][0-9]*|([1-9][0-9]*)*R))*)$/;
		var val = e.target.value;
		if (!exp.test(val)){
			return false;
		}
		updatePlusFields('wd');
		matches[2] = val;
	});
	
	$('input[name=lt-match]').on('change', function(e){
		var exp = /^|(([1-9][0-9]*|([1-9][0-9]*)*R)(\+([1-9][0-9]*|([1-9][0-9]*)*R))*)$/;
		var val = e.target.value;
		if (!exp.test(val)){
			return false;
		}
		updatePlusFields('lt');
		matches[3] = val;
	});
	
	$('input[name=dk-match]').on('change', function(e){
		var exp = /^|(([1-9][0-9]*|([1-9][0-9]*)*R)(\+([1-9][0-9]*|([1-9][0-9]*)*R))*)$/;
		var val = e.target.value;
		if (!exp.test(val)){
			return false;
		}
		updatePlusFields('dk');
		matches[4] = val;
	});
	
	$('input[name=ht-match]').on('change', function(e){
		var exp = /^|(([1-9][0-9]*|([1-9][0-9]*)*R)(\+([1-9][0-9]*|([1-9][0-9]*)*R))*)$/;
		var val = e.target.value;
		if (!exp.test(val)){
			return false;
		}
		updatePlusFields('ht');
		matches[5] = val;
	});
	
	$('#about-block').hide();
	$('#about-link').on('click', function(e){
		if (!$('#team-block').hasClass('hidden')){
			$('#team-block').fadeToggle();
			$('#results-block').fadeToggle();
			$('#about-block').delay(410).fadeToggle(205);
		}
		else {
			$('#about-block').fadeToggle();
		}
	});
	
	// This problem, and this solution, has caused a dicks amount of problems.
	$( document ).ajaxComplete( function(a, b, c){
		if (!c.crossDomain){
			return;
		}
		if (ajxRan[0] > 0 && ajxRan[0] == ajxRan[1]){
			buildTeamTable(ajxRan[2]);
		}
		else {
			ajxRan[1] += 1;
		}
	} );
} );

function buildTeam(teamData){
	var team = [];
	team.enhance = [0, 0, 0, 0, 0]; // Row enhance, per element.
	ajxRan = [6, 0, team];

	// Lookup each of the five monsters.
	lookupMonster(teamData.leader, 0, team);

	// Lookup the friend.
	if (teamData.friend_leader){
		var monster = new Object();
		monster.id = teamData.friend_leader;

		// Just look it up once.
		var monstFromList = getMonstFromList(monster.id);
		monster.name = monstFromList.name;
		monster.awakenings = monstFromList.awoken_skills.slice(0, teamData.friend_awakening);
		monster.elements = monstFromList.element2 != null ? [monstFromList.element, monstFromList.element2] : [monstFromList.element];
		monster.type = monstFromList.type2 != null ? [monstFromList.type, monstFromList.type2] : [monstFromList.type];
		monster.lead = monstFromList.leader_skill;
		monster.level = teamData.friend_level;
		monster.image = "http://www.padherder.com/" + monstFromList.image60_href;

		// MinStat + (MaxStat - MinStat) * (Level-1/MaxLevel-1)^StatGrowth
		monster.hp = statCalc(monstFromList.hp_min, monstFromList.hp_max, monster.level, monstFromList.max_level, monstFromList.hp_scale) + teamData.friend_hp * 10 + countAwaken(monster.awakenings, 1) * 200;
		monster.atk = statCalc(monstFromList.atk_min, monstFromList.atk_max, monster.level, monstFromList.max_level, monstFromList.atk_scale) + teamData.friend_atk * 5 + countAwaken(monster.awakenings, 2) * 100;
		monster.rcv = statCalc(monstFromList.rcv_min, monstFromList.rcv_max, monster.level, monstFromList.max_level, monstFromList.rcv_scale) + teamData.friend_rcv * 3 + countAwaken(monster.awakenings, 3) * 50;
		
		monster.bonus = [1, 1, 1];
		monster.boosted_stats = [monster.hp, monster.atk, monster.rcv];

		team.enhance[0] += countAwaken(monster.awakenings, 22);
		team.enhance[1] += countAwaken(monster.awakenings, 23);
		team.enhance[2] += countAwaken(monster.awakenings, 24);
		team.enhance[3] += countAwaken(monster.awakenings, 25);
		team.enhance[4] += countAwaken(monster.awakenings, 26);
		
		ajxRan[1] += 1;
		team[5] = monster;
	}
	else {
		ajxRan[0] -= 1;
		team[5] = undefined;
	}
	
	for (var i = 1; i <= 4; i++){
		if (teamData['sub' + i]){
			lookupMonster(teamData['sub' + i], i, team);
		}
		// Eh, let's keep the size of the list constant.
		else {
			ajxRan[0] -= 1;
			team[i] == undefined;
		}
	}; 
}

function buildTeamTable(team){
	console.log("Building display table...");
	$('#team-block').addClass('hidden');
	var leadSkill = [];
	var friendSkill = [];
	
	// Calc the Leader Skill bonus.
	for (var i = 0; i < lead_skills.length; i++){
		var curr = lead_skills[i]
		if (!leadSkill.length && curr.name == team[0].lead){
			leadSkill = curr.hasOwnProperty('data') ? curr.data : [1, 1, 1];
		}
		if (!friendSkill.length && curr.name == team[5].lead){
			friendSkill = curr.hasOwnProperty('data') ? curr.data : [1, 1, 1];
		}
	}
	
	function applyLeads(lead, monster){
		// If there's no condition, apply it and move on.
		if (lead.length == 3){
			monster.bonus = [ monster.bonus[0] * lead[0], monster.bonus[1] * lead[1], monster.bonus[2] * lead[2] ];
			return;
		}
	
		for (var j = 3; j < lead.length; j++){
			// If either condition is met, apply the multiplier and bugger off.
			if (lead[j][0] == "elem"){
				for (var k = 1; (k < lead[j].length && monster.elements.indexOf(lead[j][k]) != -1); k++){
					monster.bonus = [ monster.bonus[0] * lead[0], monster.bonus[1] * lead[1], monster.bonus[2] * lead[2] ];
					return;
				}
			}

			if (lead[j][0] == "type"){
				for (var k = 1; (k < lead[j].length && monster.type.indexOf(lead[j][k]) != -1); k++){
					monster.bonus = [ monster.bonus[0] * lead[0], monster.bonus[1] * lead[1], monster.bonus[2] * lead[2] ];
					return;
				}
			}
		}
	}
	
	function helper(stat, i){
		var key = ({'hp': 0, 'atk': 1, 'rcv': 2})[stat];
		if (team[i].bonus[key] != 1){
			$('#' + stat + '-row .team-pos' + (i+1) + ' span').text( Math.floor(team[i].boosted_stats[key]) );
			$('#' + stat + '-row .team-pos' + (i+1) + ' span').tooltip('hide').attr('title', Math.round(team[i][stat])).tooltip('fixTitle');
		}
		else {
			$('#' + stat + '-row .team-pos' + (i+1) + ' span').text( Math.round(team[i][stat]) );
		}
	}
	
	for (var i = 0; i <= 6; i++){
		if (!team[i]){
			// Use ? image.
			$('.team-img-' + (i+1)).attr('src', 'https://www.padherder.com/static/img/monsters/60x60/0.png');
			continue;
		}
		
		applyLeads(leadSkill, team[i]);
		applyLeads(friendSkill, team[i]);
		team[i].boosted_stats = [team[i].hp * team[i].bonus[0], team[i].atk * team[i].bonus[1], team[i].rcv * team[i].bonus[2]];

		$('.team-img-' + (i+1)).attr('src', team[i].image);
		$('#level-row td.team-pos' + (i+1) + ' span').text(team[i].level);
		$('#type-row td.team-pos' + (i+1) + ' span').html( team[i].type.length == 2 ? types[team[i].type[0]] + "<br/>" + types[team[i].type[1]] : types[team[i].type[0]] );
		$('#lead-row td.team-pos' + (i+1) + ' span').text("x" + team[i].bonus[0] + " x" + team[i].bonus[1] + " x" + team[i].bonus[2]);
		
		helper('hp', i);
		helper('atk', i);
		helper('rcv', i);
	}
	
	$('input[name=fr-enhance]').val(team.enhance[0]);
	$('input[name=wd-enhance]').val(team.enhance[1]);
	$('input[name=wt-enhance]').val(team.enhance[2]);
	$('input[name=lt-enhance]').val(team.enhance[3]);
	$('input[name=dk-enhance]').val(team.enhance[4]);
	
	$('[data-toggle=tooltip]').tooltip();
	$('#team-block').removeClass('hidden');
	running = false;
	return;
}

function lookupMonster(padhID, index, team){
	$.ajax( "https://www.padherder.com/user-api/monster/" + padhID + "/", {dataType: 'json', success: function(a, b, c){
		m = createMonster(a);
		team.enhance[0] += countAwaken(m.awakenings, 22);
		team.enhance[1] += countAwaken(m.awakenings, 23);
		team.enhance[2] += countAwaken(m.awakenings, 24);
		team.enhance[3] += countAwaken(m.awakenings, 25);
		team.enhance[4] += countAwaken(m.awakenings, 26);
		addMonsterToTeam( m, index, team );
	}} );
}
	
function createMonster(monsterData){
	if (!monsterData){
		console.log("Error: invalid monster data.");
		return;
	}
	console.log("Monster data retrieved. Processing...");
	
	var monster = new Object();
	monster.id = monsterData.monster;
	
	// Just look it up once.
	var monstFromList = getMonstFromList(monster.id);
	monster.name = monstFromList.name;
	monster.awakenings = monstFromList.awoken_skills.slice(0, monsterData.current_awakening);
	monster.elements = monstFromList.element2 != null ? [monstFromList.element, monstFromList.element2] : [monstFromList.element];
	monster.type = monstFromList.type2 != null ? [monstFromList.type, monstFromList.type2] : [monstFromList.type];
	monster.lead = monstFromList.leader_skill;
	monster.image = "http://www.padherder.com/" + monstFromList.image60_href;
	
	// 1 + MaxLvl * ( (XP/Max)^(1/2.5) )
	monster.level = 1 + Math.floor((monstFromList.max_level - 1) * Math.pow((monsterData.current_xp/xpAtLevel(monstFromList.xp_curve, monstFromList.max_level)), 1/2.5));
	
	// MinStat + (MaxStat - MinStat) * (Level-1/MaxLevel-1)^StatGrowth
	monster.hp = statCalc(monstFromList.hp_min, monstFromList.hp_max, monster.level, monstFromList.max_level, monstFromList.hp_scale) + monsterData.plus_hp * 10 + countAwaken(monster.awakenings, 1) * 200;
	monster.atk = statCalc(monstFromList.atk_min, monstFromList.atk_max, monster.level, monstFromList.max_level, monstFromList.atk_scale) + monsterData.plus_atk * 5 + countAwaken(monster.awakenings, 2) * 100;
	monster.rcv = statCalc(monstFromList.rcv_min, monstFromList.rcv_max, monster.level, monstFromList.max_level, monstFromList.rcv_scale) + monsterData.plus_rcv * 3 + countAwaken(monster.awakenings, 3) * 50;
	monster.bonus = [1, 1, 1];
	monster.boosted_stats = [monster.hp, monster.atk, monster.rcv];
	
	return monster;
}

function getMonstFromList(id){
	var index = id;
	var monster = monst_list[index];
	while (monster.id != id){
		diff = monster.id - index;
		index = diff > 0 ? index - 1 : index + 1;
		monster = monst_list[index];
	}
	return monster;
}

function addMonsterToTeam(monster, index, team){
	team[index] = monster;
	console.log("Monster processed.");
}

function xpAtLevel(curve, level){
	return Math.round(curve * Math.pow((level-1)/98, 2.5));
}

function statCalc(min, max, level, mlvl, growth){
	var stat = min + ((max-min) * Math.pow((level-1)/(mlvl-1), growth));
	return stat;
}

function countAwaken(awakens, id){
	var cnt = 0;
	var awkn = awakens;
	while (awkn.indexOf(id) != -1){
		cnt++;
		awkn = awkn.slice( awkn.indexOf(id)+1 );
	}
	return cnt;
}

function updatePlusFields(clr){
	var contents = $('input[name=' + clr + '-match]').val();
	
	var combos = contents.split('+');
	var str = "";
	for (var i = 0; i < combos.length; i++){
		//str += i > 0 ? "+" + eval( combos[i].replace('R', '+6') ) : eval( combos[i].replace('R', '+6') );
		// Derp, default to 0. Assume no plus orbs in the combo.
		str += i > 0 ? "+" + 0 : 0;
	}
	
	$('input[name=' + clr + '-plus]').val(str);
}

function calcDamage(){
	// elem1 = defending, elem2 = attacking
	function getElemMult(elem1, elem2){
		if ((elem1 == 0 && elem2 == 1) || (elem1 == 1 && elem2 == 2) || (elem1 == 2 && elem2 == 0) || (elem1 == 3 && elem2 == 4) || (elem1 == 4 && elem2 == 3)){
			return 2;
		}
		if ((elem2 == 0 && elem1 == 1) || (elem2 == 1 && elem1 == 2) || (elem2 == 2 && elem1 == 0) || (elem2 == 3 && elem1 == 4) || (elem2 == 4 && elem1 == 3)){
			return 0.5;
		}
		return 1;
	}

	function getActiveMult(skill, monster, elem){
		elem = typeof elem !== 'undefined' ? elem : -1;
		for (var j = 3; j < skill.length; j++){
			if (skill[j][0] == "elem"){
				for (var k = 1; (k < skill[j].length && monster.elements.indexOf(skill[j][k]) != -1 && skill[j].indexOf(elem) != -1); k++){
					return skill[1];
				}
			}
			if (skill[j][0] == "type"){
				for (var k = 1; (k < skill[j].length && monster.type.indexOf(skill[j][k]) != -1); k++){
					return skill[1];
				}
			}
		}
		return 1;
	}
	
	// TODO: Drop "" empty combos from the list, instead of checking everywhere for them.

	// Damage = (Active * Typing * Board * Row Enhance * Attack) - Defense
	var team = ajxRan[2];
	var t_damage = [[], [], [], [], [], []];
	var raw_damage = [0, 0, 0, 0, 0];
	var match_explode = new Array(6);
	var plus_orbs = [0, 0, 0, 0, 0];
	var elem = $('#enemy-elem').val().slice(-1);
	var health = $('#enemy-health').val();
	var defense = $('#enemy-def').val();
	
	// Load plus orb data from fields.
	plus_orbs[0] = $('input[name=fr-plus]').val().split('+');
	plus_orbs[1] = $('input[name=wt-plus]').val().split('+');
	plus_orbs[2] = $('input[name=wd-plus]').val().split('+');
	plus_orbs[3] = $('input[name=lt-plus]').val().split('+');
	plus_orbs[4] = $('input[name=dk-plus]').val().split('+');

	// Loading active skill multiplier and conditions.
	var act_skl = [1, $('#active-field').val(), 1];
	act_skl[1] = isNaN(act_skl[1])? 1 : eval(act_skl[1]);
	var cond1 = $('select[name=act-cond1]').val().split('-');
	var cond2 = $('select[name=act-cond2]').val().split('-');
	
	if ((cond1[0] == cond2[0]) && cond1[0]){
		act_skl.push( [cond1[0], eval(cond1[1]), eval(cond2[1])] );
	}
	else {
		if (cond1[0]){
			act_skl.push( [cond1[0], eval(cond1[1])] );
		}
		if (cond2[0]){
			act_skl.push( [cond2[0], eval(cond2[1])] );
		}
	}
	
	// BOARD
	var combo_count = 0;
	for (var i = 0; i <= 5; i++){
		match_explode[i] = matches[i].split('+');
		combo_count += match_explode[i].length;
		if (match_explode[i].length == 1 && (match_explode[i] == "" || match_explode[i] < 3)){
			combo_count -= 1;
		}
	}
	if (combo_count == 0){ return -1; }
	
	var combo_contrib = 1 + ((combo_count - 1)/4);
	for (var i = 0; i <= 4; i++){
		var rows = 0;
		var colour_contrib = 0;
		var tpa_section = 0;
		for (var j = 0; j < match_explode[i].length; j++){
			var curr_combo = match_explode[i][j];
			if (curr_combo == "" || curr_combo < 3){
				continue;
			}
			if (curr_combo.search(/R/) != -1){
				rows++;
				curr_combo = curr_combo.replace(/R/, '+6');
			}
			// 6% per plus orb in the combo
			var p_o = eval(plus_orbs[i][j]);
			p_o = isNaN(p_o) ? 0 : p_o;
			if (curr_combo == 4){
				tpa_section += (1 + (p_o * 0.06)) * (1 + ((eval(curr_combo)-3)/4));
			}
			else {
				colour_contrib += (1 + (p_o * 0.06)) * (1 + ((eval(curr_combo)-3)/4));
			}
		}
		
		var row_contrib = 1 + (0.1 * rows * team.enhance[i]);
		raw_damage[i] = [(combo_contrib * colour_contrib * row_contrib), (combo_contrib * tpa_section * row_contrib)];
	}
	
	var t_dam = 0;
	for (var i = 0; i < 6; i++){
		if (!team[i]){
			$('#results-block .team-img-' + (i+1) + ' ~ .dmg-' + (1)).text(0);
			continue;
		}
		
		for (var j = 0; j < team[i].elements.length; j++){
			var curr_elem = team[i].elements[j];
			var elem_bonus = getElemMult(elem, curr_elem);
			var attack = team[i].boosted_stats[1];
			var mult_bonus = getActiveMult(act_skl, team[i], curr_elem);
			
			// If there's no damage for this element, put a 0 and move on.
			if ((raw_damage[curr_elem][0] + raw_damage[curr_elem][1]) == 0){
				$('#results-block .team-img-' + (i+1) + ' ~ .dmg-' + (j+1)).text(0);
				continue;
			}
			
			// Sub-element check.
			attack = j == 1 ? (team[i].elements[0] == team[i].elements[1] ? attack * 0.1 : attack * 0.3) : attack;
			var tpa_bonus = Math.pow(1.5, countAwaken(team[i].awakenings, 27));
			var dam = (mult_bonus * (raw_damage[ curr_elem ][0] + (raw_damage[ curr_elem ][1] * tpa_bonus)) * elem_bonus * attack) - defense;
			dam = Math.max(1, Math.round(dam));
			$('#results-block .team-img-' + (i+1) + ' ~ .dmg-' + (j+1)).text(dam);
			t_dam += dam;
		}
	}
	
	$('#results-block #tdam-result').text( t_dam );
	$('#results-block #calc-result').text( t_dam >= health ? 'Spiked! 0 HP!' : (health - t_dam) + " HP" );
	$('#results-block').removeClass('hidden');
}